#include <stdio.h>
#define size 10
//This program add and multiply two matrices

//checks whether we can add given matrices
int addCheck(int R1,int R2,int C1,int C2);

//checks whether we can multiply given matrices
int multiCheck(int R1,int R2,int C1,int C2);

int addCheck(int R1,int R2,int C1,int C2){
	if (!(R1==R2 && C1==C2))
		return 0;
	else
		return 1;
}

int multiCheck(int R1,int R2,int C1,int C2){
	if(!(C1==R2))
		return 0;
	else
		return 1;
}

int main(){
	
	int r1,r2,c1,c2,c,s,e,j,m;
	char C;
	int M1[size][size],M2[size][size],M3[size][size],M4[size][size];
	
	do{
		printf("Enter number of rows and columns of first matrix \n");
		scanf("%d %d",&r1,&c1);
	
		printf("Enter number of rows and columns of second matrix \n");
		scanf("%d %d",&r2,&c2);
		
		printf("Do you want to add(+) or multiply(x) \n");
		scanf(" %c",&C);
		
		switch(C){
			case '+':c=addCheck(r1,r2,c1,c2);break;
			case 'x':c=multiCheck(r1,r2,c1,c2);break;
		}
	
		if(c==0)
			printf("Error! : operation is not possible\n");
		else if(c==1)
			break;	
		
	}while(1);
	
	printf("Enter elements of 1st matrix\n");
	for(s=0;s<r1;s++)//takes elements of 1st matrix
	{
		for(e=0;e<c1;e++)
		{
			scanf("%d",&M1[s][e]);
		}
	}
	
	printf("\n");
	
	printf("Enter elements of 2st matrix\n");
	for(s=0;s<r2;s++)//takes elements of 2nd matrix
	{
		for(e=0;e<c2;e++)
		{
			scanf("%d",&M2[s][e]);
		}
	}
	
	printf("\nanswer:\n");
		
	if(C=='+'){
		for(s=0;s<r1;s++)//adds two matrixes
		{
			for(e=0;e<c1;e++)
			{
				M3[s][e]=M1[s][e]+M2[s][e];
				printf("%d ",M3[s][e]);	
			}
			printf("\n");
		}
	}

	
	if(C=='x'){
		for(s=0;s<r1;s++)//multiplies two matrixes
		{
			for(e=0;e<c2;e++)
			{
				M4[s][e]=0;
				for(j=0;j<c1;j++)
				{
					m=M1[s][j]*M2[j][e];
					M4[s][e]+=m;
				}
				printf("%d ",M4[s][e]);
			}
			printf("\n");
		}
	}
}
	

	
	
	
